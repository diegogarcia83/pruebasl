﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPruebaSL.Modelos;

namespace WebPruebaSL.Paginas
{
    public partial class Vinculaciones : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargarVinculaciones(Request.QueryString["id"].ToString());
                }

                if (Request.QueryString["nombres"] != null)
                {
                    h2Titulo.InnerText = string.Format("Vinculaciones de {0}",(Request.QueryString["nombres"].ToString()));
                }
            }
        }

        private async void CargarVinculaciones(string id)
        {
            string path = "https://localhost:44307/api/VinculacionesPorEmpleado/" + id;
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var aux = await response.Content.ReadAsStringAsync();
                var jsonSerialization = new JavaScriptSerializer();
                var list = jsonSerialization.Deserialize<List<VinculacionModel>>(aux);
                gvVinculaciones.DataSource = list;
                gvVinculaciones.DataBind();
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            GridViewRow seleccionada = (GridViewRow)button.NamingContainer;
            string id = seleccionada.Cells[0].Text;
            string idEmpleado = Request.QueryString["id"].ToString();
            string path = string.Format("~/Paginas/VinculacionDetalle.aspx?id={0}&idEmpleado={1}&nombres={2}", id, idEmpleado, Request.QueryString["nombres"].ToString());
            Response.Redirect(path);
        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            string idEmpleado = Request.QueryString["id"].ToString();
            string path = string.Format("~/Paginas/VinculacionDetalle.aspx?idEmpleado={0}&nombres={1}", idEmpleado, Request.QueryString["nombres"].ToString());
            Response.Redirect(path);

        }

        protected async void btnEliminar_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            GridViewRow seleccionada = (GridViewRow)button.NamingContainer;
            string id = seleccionada.Cells[0].Text;
            string path = "https://localhost:44307/api/Vinculaciones/" + id;
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.DeleteAsync(path);
            string pathVolver = string.Format("~/Paginas/Vinculaciones.aspx?id={0}&nombres={1}", Request.QueryString["id"].ToString(), Request.QueryString["nombres"].ToString());
            Response.Redirect(pathVolver);
        }

        
    }
}