﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPruebaSL.Modelos;

namespace WebPruebaSL.Paginas
{
    public partial class EmpleadoDetalle : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                await cargarCiudades();
                await cargarGeneros();
                if (Request.QueryString["id"] != null)
                {
                    CargarDatos(Request.QueryString["id"].ToString());
                }
            }
        }

        private async void CargarDatos(string id)
        {
            string path = "https://localhost:44307/api/Empleados/" + id;
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var aux = await response.Content.ReadAsStringAsync();
                var jsonSerialization = new JavaScriptSerializer();
                var empleado = jsonSerialization.Deserialize<EmpleadoModel>(aux);
                txtApellido.Text = empleado.Apellidos;
                txtNombre.Text = empleado.Nombres;
                txtEmail.Text = empleado.Email;
                txtDireccion.Text = empleado.Direccion;
                ddlCiudad.SelectedValue = empleado.IdCiudad.ToString();
                ddlGenero.SelectedValue = empleado.IdGenero.ToString();
            }
        }

        private async Task<bool> cargarCiudades()
        {
            string path = "https://localhost:44307/api/Ciudades";
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var aux = await response.Content.ReadAsStringAsync();
                var jsonSerialization = new JavaScriptSerializer();
                var list = jsonSerialization.Deserialize<List<CiudadModel>>(aux);
                ddlCiudad.DataSource = list;
                ddlCiudad.DataBind();
            }
            return true;
        }

        private async Task<bool> cargarGeneros()
        {
            string path = "https://localhost:44307/api/Generos";
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var aux = await response.Content.ReadAsStringAsync();
                var jsonSerialization = new JavaScriptSerializer();
                var list = jsonSerialization.Deserialize<List<GeneroModel>>(aux);
                ddlGenero.DataSource = list;
                ddlGenero.DataBind();
            }
            return true;
        }

        protected async void btnGuardar_Click(object sender, EventArgs e)
        {
            EmpleadoModel empleadoModel = new EmpleadoModel
            {
                Apellidos = txtApellido.Text,
                Nombres = txtNombre.Text,
                Direccion = txtDireccion.Text,
                Email = txtEmail.Text,
                IdCiudad = Convert.ToInt32(ddlCiudad.SelectedValue),
                IdGenero = Convert.ToInt32(ddlGenero.SelectedValue)
            };
            var jsonSerialization = new JavaScriptSerializer();
            
            HttpClient client = new HttpClient();
            if (Request.QueryString["id"] != null)
            {
                string id = Request.QueryString["id"].ToString();
                empleadoModel.IdEmpelado = Convert.ToInt32(id);
                string json = jsonSerialization.Serialize(empleadoModel);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync("https://localhost:44307/api/Empleados/" + id, content);
                response.EnsureSuccessStatusCode();
            }
            else
            {
                string json = jsonSerialization.Serialize(empleadoModel);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("https://localhost:44307/api/Empleados", content);
                response.EnsureSuccessStatusCode();
            }
            Response.Redirect("Empleado.aspx");
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Empleado.aspx");
        }
    }
}