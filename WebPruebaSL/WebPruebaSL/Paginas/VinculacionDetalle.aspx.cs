﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPruebaSL.Modelos;

namespace WebPruebaSL.Paginas
{
    public partial class VinculacionDetalle : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                await cargarCargos();
                if (Request.QueryString["id"] != null)
                {
                    CargarDatos(Request.QueryString["id"].ToString());
                }
            }

        }


        private async void CargarDatos(string id)
        {
            string path = "https://localhost:44307/api/Vinculaciones/" + id;
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var aux = await response.Content.ReadAsStringAsync();
                var jsonSerialization = new JavaScriptSerializer();
                var vinculacion = jsonSerialization.Deserialize<VinculacionModel>(aux);
                txtFechaIngreso.Text = vinculacion.FechaIngreso.ToString("yyyy-MM-dd");
                txtFechaRetiro.Text = vinculacion.FechaRetiro.HasValue ? vinculacion.FechaRetiro.Value.ToString("yyyy-MM-dd") : null;
            }
        }

        private async Task<bool> cargarCargos()
        {
            string path = "https://localhost:44307/api/Cargos";
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var aux = await response.Content.ReadAsStringAsync();
                var jsonSerialization = new JavaScriptSerializer();
                var list = jsonSerialization.Deserialize<List<CargoModel>>(aux);
                ddlCargo.DataSource = list;
                ddlCargo.DataBind();
            }
            return true;
        }

        protected async void btnGuardar_Click(object sender, EventArgs e)
        {
            int idEmpleado = Convert.ToInt32(Request.QueryString["idEmpleado"].ToString());
            VinculacionModel vinculacionModel = new VinculacionModel
            {
                IdEmpelado = idEmpleado,
                FechaIngreso = Convert.ToDateTime(string.Format("{0} 00:00:00", txtFechaIngreso.Text)),
                FechaRetiro = string.IsNullOrEmpty(txtFechaRetiro.Text) ? (DateTime?)null : Convert.ToDateTime(string.Format("{0} 00:00:00", txtFechaRetiro.Text)),
                IdCargo = Convert.ToInt32(ddlCargo.SelectedValue)
            };
            var jsonSerialization = new JavaScriptSerializer();

            HttpClient client = new HttpClient();
            if (Request.QueryString["id"] != null)
            {
                string id = Request.QueryString["id"].ToString();
                vinculacionModel.IdEmpelado = Convert.ToInt32(id);
                string json = jsonSerialization.Serialize(vinculacionModel);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync("https://localhost:44307/api/Vinculaciones/" + id, content);
                response.EnsureSuccessStatusCode();
            }
            else
            {
                string json = jsonSerialization.Serialize(vinculacionModel);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("https://localhost:44307/api/Vinculaciones", content);
                response.EnsureSuccessStatusCode();
            }
            volver();
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            volver();
        }

        private void volver()
        {
            string path = string.Format("~/Paginas/Vinculaciones.aspx?id={0}&nombres={1}", Request.QueryString["idEmpleado"].ToString(), Request.QueryString["nombres"].ToString());
            Response.Redirect(path);
        }
    }
}