﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPruebaSL.Modelos;

namespace WebPruebaSL.Paginas
{
    public partial class Empleado : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CargarEmpleados();
            }
        }

        private async void CargarEmpleados()
        {
            string path = "https://localhost:44307/api/Empleados";
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var aux = await response.Content.ReadAsStringAsync();
                var jsonSerialization = new JavaScriptSerializer();
                var list = jsonSerialization.Deserialize<List<EmpleadoModel>>(aux);
                gvEmpleados.DataSource = list;
                gvEmpleados.DataBind();
            }
        }

        protected void btnVerVinculaciones_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            GridViewRow seleccionada = (GridViewRow)button.NamingContainer;
            string id = seleccionada.Cells[0].Text;
            string nombres = seleccionada.Cells[1].Text;
            string apellidos = seleccionada.Cells[2].Text;
            string path = string.Format("~/Paginas/Vinculaciones.aspx?id={0}&nombres={1}", id, string.Join(" ",  nombres, apellidos));
            Response.Redirect(path);
        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Paginas/EmpleadoDetalle.aspx");
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            GridViewRow seleccionada = (GridViewRow)button.NamingContainer;
            string id = seleccionada.Cells[0].Text;
            string path = string.Format("~/Paginas/EmpleadoDetalle.aspx?id={0}", id);
            Response.Redirect(path);
        }

        protected async void btnEliminar_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            GridViewRow seleccionada = (GridViewRow)button.NamingContainer;
            string id = seleccionada.Cells[0].Text;
            string path = "https://localhost:44307/api/Empleados/"+id;
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.DeleteAsync(path);
            Response.Redirect("Empleado.aspx");

        }
    }
}


