﻿<%@ Page Title="" Async="true" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Vinculaciones.aspx.cs" Inherits="WebPruebaSL.Paginas.Vinculaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Vinculaciones
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <form runat="server">
        <div class="mx-auto">
            <h2 runat="server" id="h2Titulo">Listado de Vinculaciones</h2>
        </div>
        <br />
        <div class="container">
            <div class="row">
                <div class="col align-selg-end">
                    <asp:Button runat="server" ID="btnCrear" CssClass="btn btn-success-sm" Text="Nueva vinculación" OnClick="btnCrear_Click" />
                </div>
            </div>
        </div>

        <br />

        <div class="container row">
            <div class="table small">
                <asp:GridView runat="server" ID="gvVinculaciones" CssClass="table table-borderless table-hover" AutoGenerateColumns="false" DataKeyNames="IdVinculacion">
                    <Columns>
                        <asp:BoundField DataField="IdVinculacion" HeaderText="Id Vinculación"
                            SortExpression="Id" />
                        <asp:BoundField DataField="Cargo" HeaderText="Cargo"
                            SortExpression="Cargo" />
                        <asp:BoundField DataField="FechaIngreso" HeaderText="Fecha ingreso"
                            SortExpression="Ingreso" />
                        <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Registro"
                            SortExpression="Registro" />
                        <asp:BoundField DataField="FechaRetiro" HeaderText="Fecha Registro"
                            SortExpression="Retiro" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button Text="Editar" runat="server" CssClass="btn form-control-sm btn-success" ID="btnEditar" OnClick="btnEditar_Click" />
                                <asp:Button Text="Eliminar" runat="server" CssClass="btn form-control-sm btn-danger" ID="btnEliminar" OnClick="btnEliminar_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>

    </form>
</asp:Content>
