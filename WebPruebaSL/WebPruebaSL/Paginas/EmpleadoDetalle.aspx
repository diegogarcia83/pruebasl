﻿<%@ Page Title="" Language="C#" Async="true" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmpleadoDetalle.aspx.cs" Inherits="WebPruebaSL.Paginas.EmpleadoDetalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Empleado Detalle
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="mx-auto">
        <h2 runat="server" id="h2Titulo">Detalle del empleado</h2>
    </div>
    <form runat="server" class="h-100 d-flex align-items-center justify-content-center">        
        <div>
            
            <div class="mb-3">
                <label class="form-label">Ciudad</label>
                <asp:DropDownList runat="server" ID="ddlCiudad" DataValueField="IdCiudad" DataTextField="Descripcion" CssClass="form-control" >
                </asp:DropDownList>
                
            </div>
            <div class="mb-3">
                <label class="form-label">Genero</label>
                <asp:DropDownList runat="server" ID="ddlGenero" DataValueField="IdGenero" DataTextField="Descripcion" CssClass="form-control" >
                </asp:DropDownList>
            </div>
            <div class="mb-3">
                <label class="form-label">Nombre</label>
                <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control" />
            </div>
            <div class="mb-3">
                <label class="form-label">Apellido</label>
                <asp:TextBox runat="server" ID="txtApellido" CssClass="form-control" />
            </div>
            <div class="mb-3">
                <label class="form-label">Dirección</label>
                <asp:TextBox runat="server" ID="txtDireccion" CssClass="form-control" />
            </div>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
            </div>
            <asp:Button Text="Guardar" runat="server" CssClass="btn btn-success" ID="btnGuardar" OnClick="btnGuardar_Click" />
            <asp:Button Text="Volver" runat="server" CssClass="btn btn-primary btn-dark" ID="btnVolver" OnClick="btnVolver_Click"/>
        </div>
    </form>
</asp:Content>
