﻿<%@ Page Title="" Async="true" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VinculacionDetalle.aspx.cs" Inherits="WebPruebaSL.Paginas.VinculacionDetalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Vinculación Detalle
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="mx-auto">
        <h2 runat="server" id="h2Titulo">Detalle de vinculación</h2>
    </div>
    <form runat="server" class="h-100 d-flex align-items-center justify-content-center">        
        <div>
            
            <div class="mb-3">
                <label class="form-label">Cargo</label>
                <asp:DropDownList runat="server" ID="ddlCargo" DataValueField="IdCargo" DataTextField="Descripcion" CssClass="form-control" >
                </asp:DropDownList>
                
            </div>
           
            <div class="mb-3">
                <label class="form-label">Fecha ingreso</label>
                <asp:TextBox runat="server" ID="txtFechaIngreso" TextMode="Date" CssClass="form-control" />
            </div>
            <div class="mb-3">
                <label class="form-label">Fecha retiro</label>
                <asp:TextBox runat="server" ID="txtFechaRetiro" TextMode="Date" CssClass="form-control" />
            </div>
            
            <asp:Button Text="Guardar" runat="server" CssClass="btn btn-success" ID="btnGuardar" OnClick="btnGuardar_Click"/>
            <asp:Button Text="Volver" runat="server" CssClass="btn btn-primary btn-dark" ID="btnVolver" OnClick="btnVolver_Click"/>
        </div>
    </form>
</asp:Content>
