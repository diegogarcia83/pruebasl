﻿<%@ Page Async="true" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Empleado.aspx.cs" Inherits="WebPruebaSL.Paginas.Empleado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Empleado
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <form runat="server">
         <div class="mx-auto">
        <h2>Listado de empleados</h2>
        </div>
        <br />
        <div class="container">
            <div class="row">
                <div class="col align-selg-end">
                    <asp:Button runat="server" ID="btnCrear" CssClass="btn btn-success-sm" Text="Crear empleado" OnClick="btnCrear_Click" />
                </div>
            </div>
        </div>

        <br />

        <div class="container row">
            <div class="table small">
                <asp:GridView runat="server" ID="gvEmpleados" CssClass="table table-borderless table-hover" AutoGenerateColumns="false" DataKeyNames="IdEmpelado" >
                     <Columns>
                        <asp:BoundField DataField="IdEmpelado" HeaderText="Id Empleado"
                            SortExpression="Id" />
                        <asp:BoundField DataField="Nombres" HeaderText="Nombres"
                            SortExpression="Nombres" />
                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                            SortExpression="Apellidos" />
                        <asp:BoundField DataField="Ciudad" HeaderText="Ciudad"
                            SortExpression="Ciudad" />
                         <asp:TemplateField>
                             <ItemTemplate>
                                 <asp:Button Text="Editar" runat="server" CssClass="btn form-control-sm btn-success" ID="btnEditar" OnClick="btnEditar_Click" />
                                 <asp:Button Text="Eliminar" runat="server" CssClass="btn form-control-sm btn-danger" ID="btnEliminar" OnClick="btnEliminar_Click" />
                                 <asp:Button Text="Vinculaciones" runat="server" CssClass="btn form-control-sm btn-info" ID="btnVerVinculaciones" OnClick="btnVerVinculaciones_Click" />                                 
                             </ItemTemplate>
                         </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>

    </form>
</asp:Content>
