﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPruebaSL.Modelos
{
    public class CargoModel
    {
        public int IdCargo { get; set; }
        public string Descripcion { get; set; }
    }
}