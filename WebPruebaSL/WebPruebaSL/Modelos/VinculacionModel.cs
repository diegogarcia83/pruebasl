﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPruebaSL.Modelos
{
    public class VinculacionModel
    {
        public int IdVinculacion { get; set; }
        public int IdEmpelado { get; set; }
        public System.DateTime FechaIngreso { get; set; }
        public int IdCargo { get; set; }
        public string Cargo { get; set; }
        public Nullable<System.DateTime> FechaRetiro { get; set; }
        public Nullable<System.DateTime> FechaRegistro { get; set; }
    }
}