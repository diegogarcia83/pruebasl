﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPruebaSL.Modelos
{
    public class CiudadModel
    {
        public int IdCiudad { get; set; }
        public string Descripcion { get; set; }

    }
}