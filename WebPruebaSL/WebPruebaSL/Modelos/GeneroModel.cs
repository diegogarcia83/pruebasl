﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPruebaSL.Modelos
{
    public class GeneroModel
    {
        public int IdGenero { get; set; }
        public string Descripcion { get; set; }
    }
}