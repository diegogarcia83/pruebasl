﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPruebaSL.Modelos
{
    public class EmpleadoModel
    {
        public int IdEmpelado { get; set; }
        public int IdGenero { get; set; }
        public int IdCiudad { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }

        
        public string Genero { get; set; }
        public string Ciudad { get; set; }


    }
}