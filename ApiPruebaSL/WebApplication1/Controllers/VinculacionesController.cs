﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.DAL;

namespace WebApplication1.Controllers
{
    public class VinculacionesController : ApiController
    {
        private PruebaSLEntities db = new PruebaSLEntities();

        // GET: api/Vinculaciones
        [Route("api/VinculacionesPorEmpleado/{idEmpleado}")]
        public IQueryable<object> GetVinculacionPorEmpleado(int idEmpleado)
        {
            return db.Vinculacion.Where(x => x.IdEmpelado == idEmpleado).Select(x => new { x.IdCargo, x.IdVinculacion, Cargo = x.Cargo.Descripcion, x.FechaRegistro, x.FechaIngreso, x.FechaRetiro});
        }

        // GET: api/Vinculaciones/5
        [ResponseType(typeof(Vinculacion))]
        public async Task<IHttpActionResult> GetVinculacion(int id)
        {
            Vinculacion vinculacion = await db.Vinculacion.FindAsync(id);
            if (vinculacion == null)
            {
                return NotFound();
            }

            return Ok(new { vinculacion.IdCargo, vinculacion.IdVinculacion, Cargo = vinculacion.Cargo.Descripcion, vinculacion.FechaRegistro, vinculacion.FechaIngreso, vinculacion.FechaRetiro });
        }

        // PUT: api/Vinculaciones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVinculacion(int id, Vinculacion vinculacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vinculacion.IdVinculacion)
            {
                return BadRequest();
            }

            db.Entry(vinculacion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VinculacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vinculaciones
        [ResponseType(typeof(Vinculacion))]
        public async Task<IHttpActionResult> PostVinculacion(Vinculacion vinculacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Vinculacion.Add(vinculacion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = vinculacion.IdVinculacion }, vinculacion);
        }

        // DELETE: api/Vinculaciones/5
        [ResponseType(typeof(Vinculacion))]
        public async Task<IHttpActionResult> DeleteVinculacion(int id)
        {
            Vinculacion vinculacion = await db.Vinculacion.FindAsync(id);
            if (vinculacion == null)
            {
                return NotFound();
            }

            db.Vinculacion.Remove(vinculacion);
            await db.SaveChangesAsync();

            return Ok(vinculacion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VinculacionExists(int id)
        {
            return db.Vinculacion.Count(e => e.IdVinculacion == id) > 0;
        }
    }
}