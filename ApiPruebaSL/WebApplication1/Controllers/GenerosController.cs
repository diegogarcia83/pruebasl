﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.DAL;

namespace WebApplication1.Controllers
{
    public class GenerosController : ApiController
    {
        private PruebaSLEntities db = new PruebaSLEntities();

        // GET: api/Generos
        public IQueryable<object> GetGenero()
        {
            return db.Genero.Select(x => new { x.IdGenero, x.Descripcion }).OrderBy(x => x.Descripcion);
        }

    }
}