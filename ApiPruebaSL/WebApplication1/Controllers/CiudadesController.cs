﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.DAL;

namespace WebApplication1.Controllers
{
    public class CiudadesController : ApiController
    {
        private PruebaSLEntities db = new PruebaSLEntities();

        // GET: api/Ciudades
        
        public IQueryable<object> GetCiudad()
        {
            return db.Ciudad.Select(x=>new { x.IdCiudad, x.Descripcion }).OrderBy(x=>x.Descripcion);
        }

    }
}