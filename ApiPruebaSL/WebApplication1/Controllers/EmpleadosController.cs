﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.DAL;

namespace WebApplication1.Controllers
{
    public class EmpleadosController : ApiController
    {
        private PruebaSLEntities db = new PruebaSLEntities();

        // GET: api/Empleados
        public IQueryable<object> GetEmpleado()
        {
            return db.Empleado.Select(x => new
            {
                x.IdEmpelado,
                x.IdCiudad,
                Ciudad = x.Ciudad.Descripcion,
                x.Email,
                x.Nombres,
                x.Apellidos,
                x.Direccion,
                x.IdGenero,
                Genero = x.Genero.Descripcion
            });
        }

        // GET: api/Empleados/5
        [ResponseType(typeof(object))]
        public async Task<IHttpActionResult> GetEmpleado(int id)
        {
            Empleado empleado = await db.Empleado.FindAsync(id);
            if (empleado == null)
            {
                return NotFound();
            }

            return Ok(new { empleado.Nombres, empleado.IdEmpelado, empleado.Apellidos, empleado.Direccion, empleado.IdCiudad, empleado.IdGenero, empleado.Email});
        }

        // PUT: api/Empleados/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEmpleado(int id, Empleado empleado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != empleado.IdEmpelado)
            {
                return BadRequest();
            }

            db.Entry(empleado).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpleadoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Empleados
        [ResponseType(typeof(Empleado))]
        public async Task<IHttpActionResult> PostEmpleado(Empleado empleado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Empleado.Add(empleado);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = empleado.IdEmpelado }, empleado);
        }

        // DELETE: api/Empleados/5
        [ResponseType(typeof(Empleado))]
        public async Task<IHttpActionResult> DeleteEmpleado(int id)
        {
            Empleado empleado = await db.Empleado.FindAsync(id);
            if (empleado == null)
            {
                return NotFound();
            }

            db.Empleado.Remove(empleado);
            await db.SaveChangesAsync();

            return Ok(empleado);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmpleadoExists(int id)
        {
            return db.Empleado.Count(e => e.IdEmpelado == id) > 0;
        }
    }
}