﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.DAL;

namespace WebApplication1.Controllers
{
    public class CargosController : ApiController
    {
        private PruebaSLEntities db = new PruebaSLEntities();

        // GET: api/Cargos
        public IQueryable<object> GetCargo()
        {
            return db.Cargo.Select(x => new { x.IdCargo, x.Descripcion }).OrderBy(x => x.Descripcion); ;
        }

    }
}