//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vinculacion
    {
        public int IdVinculacion { get; set; }
        public int IdEmpelado { get; set; }
        public System.DateTime FechaIngreso { get; set; }
        public int IdCargo { get; set; }
        public Nullable<System.DateTime> FechaRetiro { get; set; }
        public Nullable<System.DateTime> FechaRegistro { get; set; }
    
        public virtual Cargo Cargo { get; set; }
        public virtual Empleado Empleado { get; set; }
    }
}
