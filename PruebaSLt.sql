USE [PruebaSL]
GO
/****** Object:  Table [dbo].[Cargo]    Script Date: 21/10/2022 10:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cargo](
	[IdCargo] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Cargo] PRIMARY KEY CLUSTERED 
(
	[IdCargo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ciudad]    Script Date: 21/10/2022 10:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ciudad](
	[IdCiudad] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Ciudad] PRIMARY KEY CLUSTERED 
(
	[IdCiudad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 21/10/2022 10:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[IdEmpelado] [int] IDENTITY(1,1) NOT NULL,
	[IdGenero] [int] NOT NULL,
	[IdCiudad] [int] NOT NULL,
	[Nombres] [nvarchar](50) NOT NULL,
	[Apellidos] [nvarchar](50) NOT NULL,
	[Direccion] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[IdEmpelado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Genero]    Script Date: 21/10/2022 10:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genero](
	[IdGenero] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Genero] PRIMARY KEY CLUSTERED 
(
	[IdGenero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vinculacion]    Script Date: 21/10/2022 10:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vinculacion](
	[IdVinculacion] [int] IDENTITY(1,1) NOT NULL,
	[IdEmpelado] [int] NOT NULL,
	[FechaIngreso] [datetime] NOT NULL,
	[IdCargo] [int] NOT NULL,
	[FechaRetiro] [datetime] NULL,
	[FechaRegistro] [datetime] NULL,
 CONSTRAINT [PK_Vinculacion] PRIMARY KEY CLUSTERED 
(
	[IdVinculacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cargo] ON 

INSERT [dbo].[Cargo] ([IdCargo], [Descripcion]) VALUES (1, N'Cargo1')
INSERT [dbo].[Cargo] ([IdCargo], [Descripcion]) VALUES (2, N'Cargo2')
INSERT [dbo].[Cargo] ([IdCargo], [Descripcion]) VALUES (3, N'Cargo3')
SET IDENTITY_INSERT [dbo].[Cargo] OFF
GO
SET IDENTITY_INSERT [dbo].[Ciudad] ON 

INSERT [dbo].[Ciudad] ([IdCiudad], [Descripcion]) VALUES (1, N'Bogotá')
INSERT [dbo].[Ciudad] ([IdCiudad], [Descripcion]) VALUES (2, N'Pasto')
SET IDENTITY_INSERT [dbo].[Ciudad] OFF
GO
SET IDENTITY_INSERT [dbo].[Empleado] ON 

INSERT [dbo].[Empleado] ([IdEmpelado], [IdGenero], [IdCiudad], [Nombres], [Apellidos], [Direccion], [Email]) VALUES (1, 1, 1, N'Diego', N'Meza', N'Av Siempre Viva 123', N'dmmgs83@hotmail.com')
SET IDENTITY_INSERT [dbo].[Empleado] OFF
GO
SET IDENTITY_INSERT [dbo].[Genero] ON 

INSERT [dbo].[Genero] ([IdGenero], [Descripcion]) VALUES (1, N'Masculino')
INSERT [dbo].[Genero] ([IdGenero], [Descripcion]) VALUES (2, N'Femenino')
INSERT [dbo].[Genero] ([IdGenero], [Descripcion]) VALUES (3, N'Otro')
INSERT [dbo].[Genero] ([IdGenero], [Descripcion]) VALUES (4, N'Femenino3')
SET IDENTITY_INSERT [dbo].[Genero] OFF
GO
SET IDENTITY_INSERT [dbo].[Vinculacion] ON 

INSERT [dbo].[Vinculacion] ([IdVinculacion], [IdEmpelado], [FechaIngreso], [IdCargo], [FechaRetiro], [FechaRegistro]) VALUES (7, 1, CAST(N'2022-10-15T05:00:00.000' AS DateTime), 1, NULL, CAST(N'2022-10-21T10:33:24.107' AS DateTime))
SET IDENTITY_INSERT [dbo].[Vinculacion] OFF
GO
ALTER TABLE [dbo].[Vinculacion] ADD  CONSTRAINT [DF_Vinculacion_FechaRegistro]  DEFAULT (getdate()) FOR [FechaRegistro]
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_Ciudad] FOREIGN KEY([IdCiudad])
REFERENCES [dbo].[Ciudad] ([IdCiudad])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [FK_Empleado_Ciudad]
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_Genero] FOREIGN KEY([IdGenero])
REFERENCES [dbo].[Genero] ([IdGenero])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [FK_Empleado_Genero]
GO
ALTER TABLE [dbo].[Vinculacion]  WITH CHECK ADD  CONSTRAINT [FK_Vinculacion_Cargo] FOREIGN KEY([IdCargo])
REFERENCES [dbo].[Cargo] ([IdCargo])
GO
ALTER TABLE [dbo].[Vinculacion] CHECK CONSTRAINT [FK_Vinculacion_Cargo]
GO
ALTER TABLE [dbo].[Vinculacion]  WITH CHECK ADD  CONSTRAINT [FK_Vinculacion_Empleado] FOREIGN KEY([IdEmpelado])
REFERENCES [dbo].[Empleado] ([IdEmpelado])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Vinculacion] CHECK CONSTRAINT [FK_Vinculacion_Empleado]
GO
